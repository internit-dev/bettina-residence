<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/css/main.min.css">
    <link rel="shortcut icon" href="../assets/img/favicon.png" type="image/x-icon">
    <title>Política de Privacidade - Bettina Residence</title>
</head>

<body>
    <main>
        <section id="banner-politica">
            <div class="filter"></div>
            <div class="container h-100 d-flex justify-content-center align-items-center">
                <h1 class="text-light">Política de Privacidade</h1>
            </div>
        </section>
        <section id="politica">
            <div class="container mt-5">
                <p>
                    Para o Bettina Residence é nosso compromisso promover a satisfação total
                    dos clientes, através de um serviço de atendimento próximo e personalizado,
                    mostrando os projetos desenvolvidos de forma clara. O objetivo deste
                    documento é esclarecer exatamente quais informações são coletadas dos
                    usuários do nosso site: https://bettinaresidence.com.br/ e respectivos serviços –
                    e de que maneira esses dados são utilizados. O Bettina Residence criou o
                    presente espaço para facilitar a troca de informações sobre os serviços,
                    contatos, informações relacionadas aos serviços que prestamos.
                </p>

                <p>
                    Neste espaço online, todas as suas informações pessoais recolhidas serão usadas
                    para o ajudar a tornar a visita no nosso site mais produtiva, fluida e agradável possível.
                    A garantia da confidencialidade dos dados pessoais dos utilizadores do nosso site é
                    importante para o Bettina Residence.
                </p>

                <p>
                    Todas as informações pessoais relativas a membros, assinantes, clientes ou visitantes
                    que usem o Bettina Residence serão tratadas em concordância com a Lei da Proteção
                    de Dados Pessoais de 26 de outubro de 1998 (Lei n.º 67/98). A informação pessoal
                    recolhida pode incluir o seu nome, e-mail, número de telefone fixo e/ou celular,
                    moradia, data de nascimento e/ou outros. Através desses meios de contatos,
                    consideramos entrar em contato com o usuário.
                </p>

                <p>
                    O email é utilizado para a operação de envio do material ou informação por você
                    requisitada no preenchimento do formulário. Também pode ser usado para envio de
                    Newsletters, sempre relacionadas aos nossos serviços. No entanto, o usuário pode
                    cancelar a assinatura a qualquer momento.
                </p>

                <p>
                    Os dados de download poderão ser divulgados como pesquisas e estatísticas, não
                    sendo revelada abertamente nenhuma informação pessoal, a menos que autorizada
                    explicitamente.
                </p>

                <p>
                Funcionários do Bettina Residence e as imobiliárias parceiras poderão eventualmente entrar em contato via email ou telefone para fazer pesquisas ou apresentar produtos e serviços. Segue abaixo a relação de imobiliárias parceiras:
                </p>

                <p>
                    - Amim Imóveis
                    Razão Social: AMIM CONSULTORIA IMOBILIARIA LTDA
                    CNPJ: 25.533.097/0001-29
                    Telefone: (21) 3620-0101
                    </p>
                    
                    <p>
                    - Brasil Brokers
                    Razão Social: NITEROI ADMINISTRADORA DE IMOVEIS LTDA
                    CNPJ: 03.212.056/0001-06
                    Telefone: (21) 2716-9300
                    </p>

                    <p>
                    - Lopes Self
                    Razão Social: SELF CONSULTORIA DE IMOVEIS LTDA
                    CNPJ: 11.547.766/0001-07
                    Telefone: (21) 3505-0500 / (21) 3031-6303
                    </p>
                    
                    <p>
                    - Spin Imóveis
                    Razão Social: SERPA PINTO IMOVEIS LTDA
                    CNPJ: 28.651.092/0001-99
                    Telefone: (21) 2703-6161
                </p>

                <p>
                    A navegação no site do Bettina Residence pressupõe a aceitação deste acordo de
                    privacidade. O Bettina Residence reserva-se ao direito de alterar este acordo sem
                    aviso prévio. Deste modo, recomendamos que consulte a nossa política de
                    privacidade com regularidade de forma a estar sempre atualizado.
                </p>

                <p>
                    O Bettina Residence se exime de qualquer responsabilidade por qualquer dano ou
                    prejuízo causado ao usuário em virtude de falhas no sistema operacional, no servidor
                    ou na Internet. Também se desobriga da responsabilidade por qualquer vírus que
                    possa atacar o equipamento do usuário em decorrência do acesso, utilização ou
                    navegação no site na Internet ou como a consequência da transferência de dados,
                    arquivos, imagens, textos ou áudio contidos no mesmo.
                </p>

                <h4>Os Cookies e Web Beacons</h4>

                <p>
                    Utilizamos cookies para armazenar informação, tais como as suas preferências
                    pessoais quando visita o nosso website. Isto poderá incluir um simples pop-up, ou uma
                    ligação em vários serviços que providenciamos.
                </p>

                <p>
                    Você detém o poder de desligar os seus cookies, nas opções do seu browser, ou
                    efetuando alterações nas ferramentas de programas Anti-Virus, como o Norton
                    Internet Security, por exemplo. No entanto, isso poderá alterar a forma como interage
                    com o nosso website, ou outros websites. Isso poderá afetar ou não permitir que faça
                    logins em programas, sites ou fóruns da nossa e de outras redes.
                </p>

                <h4>Sobre o compartilhamento de conteúdo nas redes sociais</h4>

                <p>
                    Ao clicar nos botões de compartilhamento de conteúdo nas mídias sociais disponíveis
                    em nossas páginas, o usuário estará publicando o conteúdo por meio de seu perfil na
                    rede selecionada. O Bettina Residence não tem acesso ao login e senha dos usuários
                    nessas redes, nem publicará conteúdo em nome do usuário sem que ele realize esta
                    ação.
                </p>
            </div>
        </section>
    </main>

    <?php require_once '../import-tags/importJs.php'; ?>
</body>

</html>