<section id="localizacao">
  <div class="container">
    <h2 class="text-center">Localização</h2>
    <h4 class="text-center">Um lugar onde natureza e todas as facilidades se encontram.</h4>
    <div class="row mapa">
      <div class="col-xl-6 col-md-6 col-12 col-mapa">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3673.898789304248!2d-43.07560498539926!3d-22.953954245325722!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9986e6b8fdb87f%3A0xd0eb17dc5f76768e!2sR.%20Pietro%20Farsoun%2C%20158%20-%20Piratininga%2C%20Niter%C3%B3i%20-%20RJ%2C%2024350-211!5e0!3m2!1spt-BR!2sbr!4v1619547808804!5m2!1spt-BR!2sbr" height="480" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
      </div>
      <div class="col-xl-6 col-md-6 col-12 col-img">
        <a href="./assets/img/mirante-de-piratininga.jpg" data-fslightbox="Localizacão">
          <img class="imagem-mapa" src="./assets/img/localizacao/mirante-de-piratininga.jpg" alt="Mirante de Piratininga">
          <p>Mirante de Piratininga</p>
        </a>
      </div>
    </div>
    <div class="row img-loc">
      <div class="col-xl col-lg-6 col-md-6">
        <a href="./assets/img/localizacao/hospital-itaipu.jpg" data-fslightbox="Localizacão">
          <img src="./assets/img/localizacao/thumbnails/hospital-itaipu.jpg" alt="Hospital de Itaipu">
          <p>Hospital Itaipu</p>
        </a>
      </div>
      <div class="col-xl col-lg-6 col-md-6">
        <a href="./assets/img/localizacao/shopping-itaipu-multicenter.jpg" data-fslightbox="Localizacão">
          <img src="./assets/img/localizacao/thumbnails/shopping-itaipu-multicenter.jpg" alt="Shopping Itaipu Multicenter">
          <p>Shopping Itaipu Multicenter</p>
        </a>
      </div>
      <div class="col-xl col-lg-6 col-md-6">
        <a href="./assets/img/localizacao/colegio-objetivo.jpg" data-fslightbox="Localizacão">
          <img src="./assets/img/localizacao/thumbnails/colegio-objetivo.jpg" alt="Colégio Objetivo Camboinhas">
          <p>Colégio Objetivo</p>
        </a>
      </div>
      <div class="col-xl col-lg-6 col-md-6">
        <a href="./assets/img/localizacao/grand-marche.jpg" data-fslightbox="Localizacão">
          <img src="./assets/img/localizacao/thumbnails/grand-marche.jpg" alt="Supermercado Gran Marche">
          <p>Supermercado Gran Marche</p>
        </a>
      </div>
      <div class="col-xl col-lg-6 col-md-6">
        <a href="./assets/img/localizacao/restaurante-maria-da-praia.jpg" data-fslightbox="Localizacão">
          <img src="./assets/img/localizacao/thumbnails/restaurante-maria-da-praia.jpg" alt="Restaurante Maria da Praia">
          <p>Restaurante Maria da Praia</p>
        </a>
      </div>
    </div>
    <div class="imagens-mobile">
      <div class="imagens">
        <a href="./assets/img/localizacao/mirante-de-piratininga.jpg" data-fslightbox="Localizacão Mobile">
          <img src="./assets/img/localizacao/mobile/mirante-de-piratininga.jpg" alt="Mirante de Piratininga">
          <p>Mirante de Piratininga</p>
        </a>
      </div>
      <div class="imagens">
        <a href="./assets/img/localizacao/hospital-itaipu.jpg" data-fslightbox="Localizacão Mobile">
          <img src="./assets/img/localizacao/mobile/hospital-itaipu.jpg" alt="Hospital de Itaipu">
          <p>Hospital Itaipu</p>
        </a>
      </div>
      <div class="imagens">
        <a href="./assets/img/localizacao/shopping-itaipu-multicenter.png" data-fslightbox="Localizacão Mobile">
          <img src="./assets/img/localizacao/mobile/shopping-itaipu-multicenter.png" alt="Shopping Itaipu Multicenter">
          <p>Shopping Itaipu Multicenter</p>
        </a>
      </div>
      <div class="imagens">
        <a href="./assets/img/localizacao/grand-marche.jpg" data-fslightbox="Localizacão Mobile">
          <img src="./assets/img/localizacao/mobile/grand-marche.jpg" alt="Supermercado Gran Marche">
          <p>Supermercado Gran Marche</p>
        </a>
      </div>
      <div class="imagens">
        <a href="./assets/img/localizacao/colegio-objetivo.jpg" data-fslightbox="Localizacão Mobile">
          <img src="./assets/img/localizacao/mobile/colegio-objetivo.jpg" alt="Colégio Objetivo Camboinhas">
          <p>Colégio Objetivo</p>
        </a>
      </div>
      <div class="imagens">
        <a href="./assets/img/localizacao/restaurante-maria-da-praia.jpg" data-fslightbox="Localizacão Mobile">
          <img src="./assets/img/localizacao/mobile/restaurante-maria-da-praia.jpg" alt="Restaurante Maria da Praia">
          <p>Restaurante Maria da Praia</p>
        </a>
      </div>
    </div>
  </div>
</section>