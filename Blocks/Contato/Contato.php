<section id="contato">
  <h2 class="text-center">Contato</h2>
  <div class="container">
    <div class="col-md-12">
      <form id="form-contato" action="">
        <p>Antecipe-se ao lançamento e aproveite esta oportunidade!</p>
        <div class="row">
          <div class="col-md-6 coluna-1">
            <input type="text" name="Nome" id="Nome" class="form-control" placeholder="Nome*" required>
            <input type="email" name="Email" id="Email" class="form-control" placeholder="Email*" required>
          </div>
          <div class="col-md-6 coluna-2">
            <input type="tel" name="Telefone" id="Telefone" class="form-control phone" placeholder="Telefone*" required>
            <textarea name="Mensagem" id="Mensagem" class="form-control" placeholder="Mensagem*" ></textarea>
            <input type="hidden" name="Tipo" value="contato">
          </div>
          <div class="col-12">
            <p class="politica"><input type="checkbox" id="input-contato">Aceito a <a href="./politica-de-privacidade/" target="_blank">Política de Privacidade</a></p>
          </div>
        </div>
        <div class="button-form">
          <div class="js-feedback"></div>
          <button type="submit" class="btn btn-primary js-btn-submit" id="button-contato" disabled>Enviar</button>
        </div>
      </form>
    </div>
  </div>
</section>