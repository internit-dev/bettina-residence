<section id="lazer">
  <div class="container">
    <div class="main-lazer">
      <h2 class="text-center">Áreas de Lazer</h2>
      <div class="carousel">
        <a class="toleft lazer-arrow">
          <img src="./assets/img/toleft.png" alt="Seta para a esquerda">
        </a>
        <a class="toright lazer-arrow">
          <img src="./assets/img/toright.png" alt="Seta para a direita">
        </a>
        <div class="owl-carousel lazer-carousel">
          <div class="item">
            <a href="./assets/img/lazer/churrasqueira.jpg" data-fslightbox="Lazer">
              <img src="./assets/img/lazer/thumbnails/churrasqueira.jpg" alt="Churrasqueira" width="100%">
              <div class="description">
                <p>Churrasqueira</p>
                <p>A arte do churrasco ganhou um palco exclusivo.</p>
              </div>
            </a>
          </div>
          <div class="item">
            <a href="./assets/img/lazer/coworking.jpg" data-fslightbox="Lazer">
              <img src="./assets/img/lazer/thumbnails/coworking.jpg" alt="Coworking" width="100%">
              <div class="description">
                <p>Coworking</p>
                <p>Produza mais e melhor sem sair do condomínio.</p>
              </div>
            </a>
          </div>
          <div class="item">
            <a href="./assets/img/lazer/fitness.jpg" data-fslightbox="Lazer">
              <img src="./assets/img/lazer/thumbnails/fitness.jpg" alt="Fitness" width="100%">
              <div class="description">
                <p>Fitness</p>
                <p>Nunca foi tão exclusivo entrar em forma.</p>
              </div>
            </a>
          </div>
          <div class="item">
            <a href="./assets/img/lazer/fitness-externo.jpg" data-fslightbox="Lazer">
              <img src="./assets/img/lazer/thumbnails/fitness-externo.jpg" alt="Fitness Externo" width="100%">
              <div class="description">
                <p>Fitness Externo</p>
                <p>Treinar ao ar livre com estrutura é ainda mais saudável.</p>
              </div>
            </a>
          </div>
          <div class="item">
            <a href="./assets/img/lazer/piscina-night.jpg" data-fslightbox="Lazer">
              <img src="./assets/img/lazer/thumbnails/piscina-night.jpg" alt="Piscina Night" width="100%">
              <div class="description">
                <p>Piscina Night</p>
                <p>Entre o mar, a lagoa e a piscina, fique com os três.</p>
              </div>
            </a>
          </div>
          <div class="item">
            <a href="./assets/img/lazer/repouso.jpg" data-fslightbox="Lazer">
              <img src="./assets/img/lazer/thumbnails/repouso.jpg" alt="Repouso" width="100%">
              <div class="description">
                <p>Repouso</p>
                <p>Depois da sauna, descanse com estilo.</p>
              </div>
            </a>
          </div>
        </div>
      </div>
      <div class="row segunda-linha mt-3">
        <div class="col-md-6 lazer-content">
          <h2>Lazer em harmonia até com o seu trabalho.</h2>
          <p>O Bettina Residence tem itens de lazer e conforto exclusivos nas áreas comuns para você relaxar e, se quiser, até trabalhar em um coworking.</p>
        </div>
        <div class="col-md-6 background position-relative">
          <img class="imagem-lazer" src="./assets/img/lazer/thumbnails/piscina-night.jpg" alt="Imagem">
          <div class="description position-absolute">
            <p>Piscina Day</p>
            <p>Entre o mar, a lagoa e a piscina, fique com os três.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>