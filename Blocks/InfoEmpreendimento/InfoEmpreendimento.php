<section id="info-empreendimento">
    <div>
        <div class="container">
            <div class="row">
              <div class="col-md-6 info-img">
                  <img src="./assets/img/piscina-day.jpg" alt="Imagem da Piscina Day">
              </div>
                <div class="col-md-6 info-empreendimento-content">
                    <div class="info-text">
                        <h2>Bettina Residence Piratininga</h2>
                        <h5 class="text-light mb-3">Apartamentos de 1 e 2 quartos com suíte, varanda gourmet e lazer exclusivo entre o mar e a lagoa.</h5>
                        <h5 class="text-light mb-3">E ainda: coberturas lineares com piscina e churrasqueira.</h5>
                        <p>O Bettina Residence captou a natureza de quem faz questão de morar com conforto e estilo sem abrir mão do contato com o meio ambiente. Aqui, você está em um paraíso cercado de belezas naturais, serviços, comércio, escolas e lazer dentro e fora do condomínio.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
