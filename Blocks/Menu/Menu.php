<header>
    <nav class="navbar navbar-expand-lg navbar-light">
        <div class="container">
            <a class="navbar-brand" href="/">
                <img src="assets/img/logo-bettina.png" alt="Logo Bettina Residence" />
            </a>
            <div class="social">
                <a href="">
                    <i class="fab fa-whatsapp"></i>
                </a>
                <a href="">
                    <i class="fab fa-instagram"></i>
                </a>
                <a href="">
                    <i class="fas fa-headset"></i>
                </a>
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navmenu" aria-controls="navmenu" aria-expanded="false" aria-label="Alterna navegação">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </button>
            <div class="collapse navbar-collapse" id="navmenu">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a href="#banner" class="nav-item active">
                            Home
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#info-empreendimento" class="nav-item">
                            O Empreendimento
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#lazer" class="nav-item">
                            Lazer
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#localizacao" class="nav-item">
                            Localização
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#plantas" class="nav-item">
                            Plantas
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#main-realizacao" class="nav-item">
                            Realização
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#contato" class="nav-item">
                            Contato
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>