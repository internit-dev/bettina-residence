<?php
$device = detect_mobile();
?>

<section id="plantas">
    <div class="container">
        <h3>Plantas</h3>

        <h4 class="text-center">Plantas que surpreendem todos os estilos.</h4>
        <div class="plantas-content">
            <div class="box-plantas">
            <?php if($device == 'desktop'): ?>
                <div class="owl-carousel plantas">
                    <div class="card-img">
                        <a href="./assets/img/plantas/bettina_tipo_01_r05.jpg" data-fslightbox="Plantas">
                            <img src="./assets/img/plantas/thumbnails/bettina_tipo_01_r05.jpg" alt="Alt da imagem" data-hash="pt1">
                            <p class="text-center">2 QUARTOS COM 1 SUÍTE, 77.01 M², COLUNA 01, VARANDA GOURMET, OPÇÃO COZINHA AMERICANA</p>
                        </a>
                    </div>
                    <div class="card-img">
                        <a href="./assets/img/plantas/bettina_tipo_02_r04.jpg" data-fslightbox="Plantas">
                            <img src="./assets/img/plantas/thumbnails/bettina_tipo_02_r04.jpg" alt="Alt da imagem" data-hash="pt2">
                            <p class="text-center">2 QUARTOS COM 1 SUÍTE, 70,75 ², COLUNA 02, VARANDA GOURMET, OPÇÃO COZINHA AMERICANA</p>
                        </a>
                    </div>
                    <div class="card-img">
                        <a href="./assets/img/plantas/bettina_tipo_03_r04.jpg" data-fslightbox="Plantas">
                            <img src="./assets/img/plantas/thumbnails/bettina_tipo_03_r04.jpg" alt="Alt da imagem" data-hash="pt3">
                            <p class="text-center">2 QUARTOS COM 1 SUÍTE, 71,03 ², COLUNA 03, VARANDA GOURMET, OPÇÃO COZINHA AMERICANA</p>
                        </a>
                    </div>
                    <div class="card-img">
                        <a href="./assets/img/plantas/bettina_tipo_04_r03.jpg" data-fslightbox="Plantas">
                            <img src="./assets/img/plantas/thumbnails/bettina_tipo_04_r03.jpg" alt="Alt da imagem" data-hash="pt4">
                            <p class="text-center">2 QUARTOS COM 1 SUÍTE, 81,95 M², COLUNA 04, VARANDA GOURMET</p>
                        </a>
                    </div>
                    <div class="card-img">
                        <a href="./assets/img/plantas/bettina_tipo_05_r04.jpg" data-fslightbox="Plantas">
                            <img src="./assets/img/plantas/thumbnails/bettina_tipo_05_r04.jpg" alt="Alt da imagem" data-hash="pt5">
                            <p class="text-center">2 QUARTOS COM 1 SUÍTE, 73,37 M², COLUNA 05, VARANDA GOURMET</p>
                        </a>
                    </div>
                    <div class="card-img">
                        <a href="./assets/img/plantas/bettina_tipo_06_r04.jpg" data-fslightbox="Plantas">
                            <img src="./assets/img/plantas/thumbnails/bettina_tipo_06_r04.jpg" alt="Alt da imagem" data-hash="pt6">
                            <p class="text-center">2 QUARTOS COM 1 SUÍTE, 63,61 M², COLUNA 06, VARANDA GOURMET, OPÇÃO COZINHA AMERICANA</p>
                        </a>
                    </div>
                    <div class="card-img">
                        <a href="./assets/img/plantas/bettina_tipo_07_r04.jpg" data-fslightbox="Plantas">
                            <img src="./assets/img/plantas/thumbnails/bettina_tipo_07_r04.jpg" alt="Alt da imagem" data-hash="pt7">
                            <p class="text-center">1 QUARTO SUÍTE, 46,88 M², COLUNA 07, VARANDA GOURMET, LAVABO</p>
                        </a>
                    </div>
                    <div class="card-img">
                        <a href="./assets/img/plantas/bettina_tipo_08_r04.jpg" data-fslightbox="Plantas">
                            <img src="./assets/img/plantas/thumbnails/bettina_tipo_08_r04.jpg" alt="Alt da imagem" data-hash="pt8">
                            <p class="text-center">1 QUARTO COM SUÍTE, 52,38 M², COLUNA 08, VARANDA GOURMET, LAVABO</p>
                        </a>
                    </div>
                    <div class="card-img">
                        <a href="./assets/img/plantas/bettina_tipo_09_r04.jpg" data-fslightbox="Plantas">
                            <img src="./assets/img/plantas/thumbnails/bettina_tipo_09_r04.jpg" alt="Alt da imagem" data-hash="pt9">
                            <p class="text-center">2 QUARTOS COM 1 SUÍTE, 66,83 M², COLUNA 09, VARANDA GOURMET</p>
                        </a>
                    </div>
                    <div class="card-img">
                        <a href="./assets/img/plantas/bettina_tipo_10_r03.jpg" data-fslightbox="Plantas">
                            <img src="./assets/img/plantas/thumbnails/bettina_tipo_10_r03.jpg" alt="Alt da imagem" data-hash="pt10">
                            <p class="text-center">2 QUARTOS COM 1 SUÍTE, 72,50 M², COLUNA 10, VARANDA GOURMET, OPÇÃO COZINHA AMERICANA</p>
                        </a>
                    </div>
                    <div class="card-img">
                        <a href="./assets/img/plantas/bettina_cob_01_r03.jpg" data-fslightbox="Plantas">
                            <img src="./assets/img/plantas/thumbnails/bettina_cob_01_r03.jpg" alt="Alt da imagem" data-hash="pt11">
                            <p class="text-center">2 QUARTOS COM 1 SUÍTE, 147,94 M², LAVABO, PISCINA, TERRAÇO E CHURRASQUEIRA</p>
                        </a>
                    </div>
                    <div class="card-img">
                        <a href="./assets/img/plantas/bettina_cob_02_r03.jpg" data-fslightbox="Plantas">
                            <img src="./assets/img/plantas/thumbnails/bettina_cob_02_r03.jpg" alt="Alt da imagem" data-hash="pt12">
                            <p class="text-center">2 SUÍTES, 128,20 M², LAVABO, PISCINA, TERRAÇO E CHURRASQUEIRA</p>
                        </a>
                    </div>
                    <div class="card-img">
                        <a href="./assets/img/plantas/bettina_cob_03_r03.jpg" data-fslightbox="Plantas">
                            <img src="./assets/img/plantas/thumbnails/bettina_cob_03_r03.jpg" alt="Alt da imagem" data-hash="pt13">
                            <p class="text-center">2 SUÍTES, 148,82 M², LAVABO, PISCINA, TERRAÇO E CHURRASQUEIRA</p>
                        </a>
                    </div>
                    <div class="card-img">
                        <a href="./assets/img/plantas/bettina_cob_04_r03.jpg" data-fslightbox="Plantas">
                            <img src="./assets/img/plantas/thumbnails/bettina_cob_04_r03.jpg" alt="Alt da imagem" data-hash="pt14">
                            <p class="text-center">2 QUARTOS COM 1 SUÍTE, 107,41 M², LAVABO, PISCINA, TERRAÇO E CHURRASQUEIRA
</p>
                        </a>
                    </div>
                </div>
            <?php else : ?>
                <div class="owl-carousel plantas">
                    <div class="card-img">
                        <a href="./assets/img/plantas/bettina_tipo_01-R05-FINAL.jpg" data-fslightbox="Plantas">
                            <img src="./assets/img/plantas/thumbnails/bettina_tipo_01-R05-FINAL.jpg" alt="Alt da imagem" data-hash="pt1">
                            <p class="text-center">2 QUARTOS COM 1 SUÍTE, 77.01 M², COLUNA 01, VARANDA GOURMET, OPÇÃO COZINHA AMERICANA</p>
                        </a>
                    </div>
                    <div class="card-img">
                        <a href="./assets/img/plantas/bettina_tipo_02_r04.jpg" data-fslightbox="Plantas">
                            <img src="./assets/img/plantas/thumbnails/bettina_tipo_02_r04.jpg" alt="Alt da imagem" data-hash="pt2">
                            <p class="text-center">2 QUARTOS COM 1 SUÍTE, 70,75 ², COLUNA 02, VARANDA GOURMET, OPÇÃO COZINHA AMERICANA</p>
                        </a>
                    </div>
                    <div class="card-img">
                        <a href="./assets/img/plantas/bettina_tipo_03_r04.jpg" data-fslightbox="Plantas">
                            <img src="./assets/img/plantas/thumbnails/bettina_tipo_03_r04.jpg" alt="Alt da imagem" data-hash="pt3">
                            <p class="text-center">2 QUARTOS COM 1 SUÍTE, 71,03 ², COLUNA 03, VARANDA GOURMET, OPÇÃO COZINHA AMERICANA</p>
                        </a>
                    </div>
                    <div class="card-img">
                        <a href="./assets/img/plantas/bettina_tipo_04_r03.jpg" data-fslightbox="Plantas">
                            <img src="./assets/img/plantas/thumbnails/bettina_tipo_04_r03.jpg" alt="Alt da imagem" data-hash="pt4">
                            <p class="text-center">2 QUARTOS COM 1 SUÍTE, 81,95 M², COLUNA 04, VARANDA GOURMET</p>
                        </a>
                    </div>
                    <div class="card-img">
                        <a href="./assets/img/plantas/bettina_tipo_05_r04.jpg" data-fslightbox="Plantas">
                            <img src="./assets/img/plantas/thumbnails/bettina_tipo_05_r04.jpg" alt="Alt da imagem" data-hash="pt5">
                            <p class="text-center">2 QUARTOS COM 1 SUÍTE, 73,37 M², COLUNA 05, VARANDA GOURMET</p>
                        </a>
                    </div>
                    <div class="card-img">
                        <a href="./assets/img/plantas/bettina_tipo_06_r04.jpg" data-fslightbox="Plantas">
                            <img src="./assets/img/plantas/thumbnails/bettina_tipo_06_r04.jpg" alt="Alt da imagem" data-hash="pt6">
                            <p class="text-center">2 QUARTOS COM 1 SUÍTE, 63,61 M², COLUNA 06, VARANDA GOURMET, OPÇÃO COZINHA AMERICANA</p>
                        </a>
                    </div>
                    <div class="card-img">
                        <a href="./assets/img/plantas/bettina_tipo_07_r04.jpg" data-fslightbox="Plantas">
                            <img src="./assets/img/plantas/thumbnails/bettina_tipo_07_r04.jpg" alt="Alt da imagem" data-hash="pt7">
                            <p class="text-center">1 QUARTO SUÍTE, 46,88 M², COLUNA 07, VARANDA GOURMET, LAVABO</p>
                        </a>
                    </div>
                    <div class="card-img">
                        <a href="./assets/img/plantas/bettina_tipo_08_r04.jpg" data-fslightbox="Plantas">
                            <img src="./assets/img/plantas/thumbnails/bettina_tipo_08_r04.jpg" alt="Alt da imagem" data-hash="pt8">
                            <p class="text-center">1 QUARTO COM SUÍTE, 52,38 M², COLUNA 08, VARANDA GOURMET, LAVABO</p>
                        </a>
                    </div>
                    <div class="card-img">
                        <a href="./assets/img/plantas/bettina_tipo_09_r04.jpg" data-fslightbox="Plantas">
                            <img src="./assets/img/plantas/thumbnails/bettina_tipo_09_r04.jpg" alt="Alt da imagem" data-hash="pt9">
                            <p class="text-center">2 QUARTOS COM 1 SUÍTE, 66,83 M², COLUNA 09, VARANDA GOURMET</p>
                        </a>
                    </div>
                    <div class="card-img">
                        <a href="./assets/img/plantas/bettina_tipo_10_r03.jpg" data-fslightbox="Plantas">
                            <img src="./assets/img/plantas/thumbnails/bettina_tipo_10_r03.jpg" alt="Alt da imagem" data-hash="pt10">
                            <p class="text-center">2 QUARTOS COM 1 SUÍTE, 72,50 M², COLUNA 10, VARANDA GOURMET, OPÇÃO COZINHA AMERICANA</p>
                        </a>
                    </div>
                    <div class="card-img">
                        <a href="./assets/img/plantas/bettina_cob_01_r03.jpg" data-fslightbox="Plantas">
                            <img src="./assets/img/plantas/thumbnails/bettina_cob_01_r03.jpg" alt="Alt da imagem" data-hash="pt11">
                            <p class="text-center">2 QUARTOS COM 1 SUÍTE, 147,94 M², LAVABO, PISCINA, TERRAÇO E CHURRASQUEIRA</p>
                        </a>
                    </div>
                    <div class="card-img">
                        <a href="./assets/img/plantas/bettina_cob_02_r03.jpg" data-fslightbox="Plantas">
                            <img src="./assets/img/plantas/thumbnails/bettina_cob_02_r03.jpg" alt="Alt da imagem" data-hash="pt12">
                            <p class="text-center">2 SUÍTES, 128,20 M², LAVABO, PISCINA, TERRAÇO E CHURRASQUEIRA</p>
                        </a>
                    </div>
                    <div class="card-img">
                        <a href="./assets/img/plantas/bettina_cob_03_r03.jpg" data-fslightbox="Plantas">
                            <img src="./assets/img/plantas/thumbnails/bettina_cob_03_r03.jpg" alt="Alt da imagem" data-hash="pt13">
                            <p class="text-center">2 SUÍTES, 148,82 M², LAVABO, PISCINA, TERRAÇO E CHURRASQUEIRA</p>
                        </a>
                    </div>
                    <div class="card-img">
                        <a href="./assets/img/plantas/bettina_cob_04_r03.jpg" data-fslightbox="Plantas">
                            <img src="./assets/img/plantas/thumbnails/bettina_cob_04_r03.jpg" alt="Alt da imagem" data-hash="pt14">
                            <p class="text-center">2 QUARTOS COM 1 SUÍTE, 107,41 M², LAVABO, PISCINA, TERRAÇO E CHURRASQUEIRA
</p>
                        </a>
                    </div>
                </div>
            <?php endif; ?>
            </div>
            <div class="box-descrip-plantas">
                <a href="#pt1" class="active">Apartamento 2 quartos - coluna 01</a>
                <a href="#pt2">Apartamento 2 quartos - coluna 02</a>
                <a href="#pt3">Apartamento 2 quartos - coluna 03</a>
                <a href="#pt4">Apartamento 2 quartos - coluna 04</a>
                <a href="#pt5">Apartamento 2 quartos - coluna 05</a>
                <a href="#pt6">Apartamento 2 quartos - coluna 06</a>
                <a href="#pt7">Apartamento 1 quarto - coluna 07</a>
                <a href="#pt8">Apartamento 1 quarto - coluna 08</a>
                <a href="#pt9">Apartamento 2 quartos - coluna 09</a>
                <a href="#pt10">Apartamento 2 quartos - coluna 10</a>
                <a href="#pt11">Cobertura 501</a>
                <a href="#pt12">Cobertura 502</a>
                <a href="#pt13">Cobertura 503</a>
                <a href="#pt14">Cobertura 504</a>
            </div>

        </div>
    </div>
</section>