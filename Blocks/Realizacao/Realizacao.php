<section id="main-realizacao">
  <h2 class="text-center">Realização</h2>
  <div class="align">
    <div class="realizacoes">
      <div class="logo-realizacoes w-100 d-flex justify-content-center">
        <div class="realizacao mr-5">
          <img src="./assets/img/realizacao/logo-ofra.png" alt="Logo Ofra Baruque" width="50%">
        </div>
        <div class="realizacao text-center ml-5">
          <img src="./assets/img/realizacao/logo-pa5.png" alt="Logo Pa5" width="80%">
        </div>
      </div>
    </div>
  </div>
</section>