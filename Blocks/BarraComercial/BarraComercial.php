<div id="barrainfo">
    <div class="container info">
        <div class="whatsapp">
            <a href="" data-toggle="modal" data-target="#modalWhatsApp">
                <img src="./assets/img/icons/whatsapp-brands.png" alt="">
                <span>Whatsapp</span>
            </a>
        </div>
        <div class="email">
            <a href="" data-toggle="modal" data-target="#modalEmail">
                <img src="./assets/img/icons/mail-send.png" alt="">
                <span>E-mail</span>
            </a>
        </div>
        <div class="topo">
            <a href="#banner">
                <img src="./assets/img/icons/chevron-circle-up-solid.png" alt="">
                <span>Topo</span>
            </a>
        </div>
    </div>
</div>

<div class="modal fade modalWhatsApp" id="modalWhatsApp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form class="js-form" id="form-whatsapp">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Whatsapp</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Coloque seu nome, número de telefone e e-mail para falar conosco por WhatsApp.</p>
                    <input type="hidden" name="Tipo" value="whatsapp">
                    <input type="text" class="form-control" placeholder="Nome*" name="Nome" id="Nome" required>
                    <input type="email" class="form-control" placeholder="E-mail*" name="Email" id="Email" required>
                    <input type="tel" class="form-control phone" placeholder="Telefone*" name="Telefone" id="Telefone" required>
                    <p class="politica mt-3"><input type="checkbox" id="input-modal-whatsapp">Aceito a <a href="./politica-de-privacidade/" target="_blank"> Política de Privacidade</a></p>
                </div>
                <div class="modal-footer">
                    <div class="js-feedback"></div>
                    <input type="hidden" name="Tipo" value="whatsapp" >
                    <button type="submit" id="button-modal-whatsapp" class="btn js-btn-submit" disabled>Enviar</button>
                    <div class="sucesso" style="color: #FFF;"></div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade modalEmail" id="modalEmail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form class="js-form" id="form-email">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Email</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Coloque seu nome, número de telefone e e-mail para falar conosco.</p>
                    <input type="hidden" name="Tipo" value="email">
                    <input type="text" class="form-control" placeholder="Nome*" name="Nome" required>
                    <input type="email" class="form-control" placeholder="E-mail*" name="Email" required>
                    <input type="tel" class="form-control phone" placeholder="Telefone*" name="Telefone" required>
                    <p class="politica mt-3"><input type="checkbox" id="input-modal-email">Aceito a <a href="./politica-de-privacidade/" target="_blank"> Política de Privacidade</a></p>
                </div>
                <div class="modal-footer">
                    <div class="js-feedback"></div>
                    <button type="submit" id="button-modal-email" class="btn js-btn-submit" disabled>Enviar</button>
                    <div class="sucesso" style="color: #FFF;"></div>
                </div>
            </form>
        </div>
    </div>
</div>