<section id="banner">
    <div class="banner-content w-100 h-100">
        <div class="container w-100 h-100 d-flex justify-content-center align-items-center">
            <div class="row w-100">
                <div class="col-xl-8 col-lg-6 col-md-6 col-ls-6 d-block header-title">
                    <div class="row">
                        <h2>Seu paraíso entre o mar e a lagoa de Piratininga</h2>
                    </div>
                    <div class="row banner-text">
                        <div class="col col-lg-4 first-col">
                            <div class="text">
                                <p>A partir de</p>
                                <p><strong>430mi*</strong></p>
                            </div>
                        </div>
                        <div class="col col-lg-8 second-col text-uppercase">
                            <p><strong>1 e 2 </strong>quartos</p>
                            <p><strong>com suíte</strong></p>
                            <p>Varanda gourmet e lazer exclusivo.</p>
                        </div>
                    </div>
                    <div class="row banner-text d-flex last-row">
                        <h4 class="banner-text text-uppercase">Cobertura com pscina.</h4>
                    </div>
                </div>
                
                <div class="col-xl-4 col-lg-6 col-md-6 col-ls-6 header-form">
                    <form action="" id="form-Banner">
                        <p>Entre em contato para saber mais</p>
                        <input type="text" class="form-control" name="Nome" id="Nome"  placeholder="Nome*" required>
                        <input type="email" class="form-control" name="Email" id="Email" placeholder="E-mail*" required>
                        <input type="tel" class="form-control phone" name="Telefone" id="Telefone" placeholder="Telefone*" required>
                        <p class="politica"><input type="checkbox" id="input-banner">Aceito a <a href="./politica-de-privacidade/" target="_blank"> Política de Privacidade</a></p>
                        <div class="submit d-flex justify-content-between align-items-center">
                            <span>*Campos obrigatórios</span>
                            <button class="btn btn-primary js-btn-submit" id="button-banner" disabled>Enviar</button>
                            <input type="hidden" name="Tipo"  value="banner">
                        </div>
                    </form>
                </div>

                <p class="right-text">*referente a unidade 101 loft</p>
            </div>
        </div>
    </div>
</section>