<!-- scripts básicos -->
<script src="assets/js/jquery-3.4.1.min.js"></script>
<script src="assets/js/jquery.mask.min.js"></script>
<script src="assets/js/intlTelInput.min.js"></script>
<script src="assets/js/main.js"></script>

<!-- PÁGINAS -->
<script src="assets/js/Blocks/Lazer/lazer.js"></script>
<script src="assets/js/Blocks/Plantas/plantas.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/fslightbox.min.js"></script>
<script src="assets/js/owl.carousel.min.js"></script>
<script src="assets/js/auxiliar.js"></script>