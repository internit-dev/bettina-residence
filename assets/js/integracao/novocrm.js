/*********************************************************************************************
 * ************************************** Funções ********************************************
 * *******************************************************************************************
*/

/**
 * 
 * @param {*} formSend 
 * @param {*} device 
 * @param {*} parameters 
 * @param {*} realtyId 
 */



function sendToCRM(formSend, device, parameters, realtyId) {

  let apiNome = $(`${formSend} [name="Nome"]`).val();
  let apiEmail = $(`${formSend} [name="Email"]`).val();
  let apiTelefone1 = $(`${formSend} [name="Telefone"]`).val();

    /*
      'apiNome': formularioDados['nome'],
      'apiEmail': formularioDados['email'],
      'apiTelefone1': formularioDados['telefoneCompleto'],
      'apiDispositivo': dispositivo,
      'apiOrigem': 'Site',
      'utm_source': formularioDados['utm_source'],
      'utm_medium': formularioDados['utm_medium'],
      'utm_campaign': formularioDados['utm_campaign'],
      */
  $.ajax({
    type: 'POST',
    url: `https://www.novocrm.atendimentoon.com.br/api/form/externo/${realtyId}`,
   // data: `apiNome=${apiNome}&apiEmail=${apiEmail}&apiTelefone1=${apiTelefone1}&apiDispositivo=${device}&${parameters}`
   data: {
    'apiNome': apiNome,
    'apiEmail': apiEmail,
    'apiTelefone1': apiTelefone1,
    'apiDispositivo': device,
    'apiOrigem': 'Site',
    'utm_source':  urlParam('utm_source'),
    'utm_medium': urlParam('utm_medium'),
    'utm_campaign': urlParam('utm_campaign') ,
   }
  })
  .done(data => {
    console.log('result: '+ data);
  })
  .always(() => {
    return true;
  });
}

/**
 * 
 * @param {*} formSend 
 */
function idEmpreendimentoNovoCRM(formSend, id)
{
  return id;
}