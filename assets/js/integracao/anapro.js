/*********************************************************************************************
 * ************************************* Parâmetros ******************************************
 * *******************************************************************************************
 */

const CampanhaKey = 'WgTvLQUcN3w1',
      ProdutoKey = 'orGQlx_0nj01',
      Key = 'dmwcNuENkDQ1',
      KeyIntegradora = "28353526-8B26-45ED-B320-2E45A39A3558",
      KeyAgencia = "79e5ccab-7046-4735-8f43-0633edb05ebd",
      CanalKey = "XdbBMfqMl7c1",
      CampanhaPeca = urlParam('utm_campaign'),
      Midia = urlParam('utm_source'),
      Peca = urlParam('utm_medium');


/*********************************************************************************************
 * ************************************** Funções ********************************************
 * *******************************************************************************************
*/

/**
 * 
 * @param {*} CampanhaKey 
 * @param {*} ProdutoKey 
 * @param {*} nome 
 * @param {*} ddd 
 * @param {*} telefone 
 * @param {*} email 
 * @param {*} mensagem 
 * @param {*} Midia 
 * @param {*} Peca 
 * @param {*} CampanhaPeca 
 * @param {*} Key 
 * @param {*} CanalKey 
 * @param {*} KeyIntegradora 
 * @param {*} KeyAgencia 
 */
function sendToAnaPro(CampanhaKey, ProdutoKey, nome, ddd, telefone, email, mensagem, 
  Midia, Peca, CampanhaPeca, Key, CanalKey, KeyIntegradora, KeyAgencia)
{
  
  dados = {
    "Key": Key,
    "TagAtalho": "",
    "CampanhaKey": CampanhaKey,
    "ProdutoKey": ProdutoKey,
    "CanalKey": CanalKey,
    "Midia": Midia,
    "Peca": Peca,
    "GrupoPeca": "",
    "CampanhaPeca": CampanhaPeca,
    "PessoaNome": nome,
    "PessoaSexo": "",
    "PessoaEmail": email,
    "PessoaTelefones": [{
        "Tipo": "OUTR",
        "DDD": ddd,
        "Numero": telefone,
        "Ramal": null
    }],
    "Observacoes": mensagem,
    "Status": "",
    "KeyExterno": "",
    "KeyIntegradora": KeyIntegradora,
    "KeyAgencia": KeyAgencia
  };
  jQuery.ajax({
    url: 'https://crm.anapro.com.br/webcrm/webapi/integracao/v2/CadastrarProspect',
    data: dados,
    crossDomain: true,
    cache: false,
    type: 'POST',
    dataType: 'json',
    success: function (response) {
      console.log(response);
      return true;
    },
    error: function(result) {
      console.log(result);
      return false;
    }
  });

}