var sections = document.querySelectorAll('section');

onscroll = function () {
  var scrollPosition = document.documentElement.scrollTop;

  sections.forEach(section => {
    if (scrollPosition >= section.offsetTop && scrollPosition < section.offsetTop + section.offsetHeight) {
      var currentId = (section.attributes.id.value);
      removeAllActiveClasses();
      addActiveClass(currentId);
    }
  });
}

var removeAllActiveClasses = function () {
  document.querySelectorAll('.navbar-nav li a').forEach(el => {
    el.classList.remove('active');
  })
}

var addActiveClass = function (id) {
  var selector = `.navbar-nav li a[href="#${id}"]`;

  document.querySelector(selector).classList.add('active');
}

//Máscara e bandeira

$(document).ready(function () {
  var phones = document.querySelectorAll('.phone');
  phones.forEach(function (phone) {
    window.intlTelInput(phone, {
      initialCountry: 'br',
    });
  });

  // telefone no formato (00) 00000-0000
  var SPMaskBehavior = function (val) {
      return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
    },
    spOptions = {
      onKeyPress: function (val, e, field, options) {
        field.mask(SPMaskBehavior.apply({}, arguments), options);
      }
    };

  $('.phone').mask(SPMaskBehavior, spOptions);

  $('.iti__country-list li').click(function () {

    setTimeout(function () {
      var title = $("#title-band").attr("title");
      var placeholder = $(".phone").attr("placeholder");

      tamanho = placeholder.length;

      if (title === "Brazil (Brasil): +55") {
        $('.phone').mask(SPMaskBehavior, spOptions);
      } else {
        $('.phone').mask('0000000000');
      }

    }, 300);
  });

  $(window).on('scroll', function () {
    var scroll = $(window).scrollTop();
    if (scroll < 100) {
      $('.navbar').removeClass('sticky-header');
      $('.navbar-brand').removeClass('sticky-header-brand');
      $(".navbar-brand img").removeClass("sticky-header-img");
      $(".navbar-nav .nav-item a").removeClass("sticky-header-item");
      $(".navbar-toggler span").removeClass("mobile");
      $(".social a i").removeClass("mobile");
    } else {
      $(".navbar").addClass("sticky-header");
      $(".navbar-brand").addClass("sticky-header-brand");
      $(".navbar-brand img").addClass("sticky-header-img");
      $(".navbar-nav .nav-item a").addClass("sticky-header-item");
      $(".navbar-toggler span").addClass("mobile");
      $(".social a i").addClass("mobile");
    }
  });

  var $doc = $('html, body');
  $('.nav-item').click(function () {
    $doc.animate({
      scrollTop: $($.attr(this, 'href')).offset().top
    }, 500);
    return false;
  });

  $("#input-banner").on("click", function (e) {
    if ($("#input-banner").is(":checked") == true) {
      $("#button-banner").attr('disabled', false);
    } else {
      $("#button-banner").attr('disabled', true)
    }
  });

  $("#input-contato").on("click", function (e) {
    if ($("#input-contato").is(":checked") == true) {
      $("#button-contato").attr('disabled', false);
    } else {
      $("#button-contato").attr('disabled', true)
    }
  });

  $("#input-modal-whatsapp").on("click", function (e) {
    if ($("#input-modal-whatsapp").is(":checked") == true) {
      $("#button-modal-whatsapp").attr('disabled', false);
    } else {
      $("#button-modal-whatsapp").attr('disabled', true)
    }
  });

  $("#input-modal-email").on("click", function (e) {
    if ($("#input-modal-email").is(":checked") == true) {
      $("#button-modal-email").attr('disabled', false);
    } else {
      $("#button-modal-email").attr('disabled', true)
    }
  });
});

document.addEventListener('DOMContentLoaded', function () {
  if (document.querySelector('.lazer-carousel')) {
    setTimeout(function () {
      //Pega os itens do carrossel
      let slide = document.querySelectorAll('.lazer-carousel .owl-item');
      //Pega o div pai que envolve as divs filho
      let slidetotal = document.querySelectorAll('.lazer-carousel');
      let ultimo = slide.length - 1;

      //Seleciona todas as duas setas
      let arrows = document.querySelectorAll('.lazer-arrow');

      //Function para remover as setas de acordo com a posição dos itens
      function remover() {
        if (slide[0].classList.contains('active')) {
          arrows[0].classList.add('d-none');
        } else if (slide[ultimo].classList.contains('active')) {
          arrows[1].classList.add('d-none');
        } else {
          arrows[0].classList.remove('d-none');
          arrows[1].classList.remove('d-none');
        }
      }

      remover();

      //Remover as setas pelo click nas setas
      for (let itens of arrows) {
        itens.onclick = function () {
          remover();
        };
      }

      //Remover a setas ao arrastar o slide para o lado
      for (let itens of slidetotal) {
        itens.ondrag = function () {
          remover();
        }
      }
    }, 300);
  }

  let sectionCookies = document.querySelector('.modalPopup');
  let buttonCookies = document.querySelector('.activeCookies');

  buttonCookies.addEventListener('click', () => {
    sessionStorage.setItem('cookie', true);
    sectionCookies.classList.add('invisible');
  });

  if (sessionStorage.getItem('cookie')) {
    sectionCookies.classList.add('invisible');
  }
});