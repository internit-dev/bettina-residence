$( document ).ready(function() {
    $('.owl-carousel.plantas').owlCarousel({
        margin:10,
        nav:false,
        items:1,
        URLhashListener:true,
        autoplayHoverPause:true,
        startPosition: 'URLHash'
    }).on('translated.owl.carousel', function(event) {
        let slide = $(this).find(".owl-item.active img").attr('data-hash');
        $('.box-descrip-plantas a').removeClass('active');
        $('.box-descrip-plantas a[href$='+slide+']').addClass('active');
    });

});