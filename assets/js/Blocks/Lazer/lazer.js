$( document ).ready(function() {
    $('.lazer-carousel').owlCarousel({
        margin:10,
        nav:false,
        items:7,
        dots: false,
        responsive:{
            320: {
                items: 1
            },
            576: {
                items: 2
            }
        }
    });

    $('.toright').click(function () {
        $('.lazer-carousel').trigger('next.owl.carousel');
    });
    $('.toleft').click(function () {
        $('.lazer-carousel').trigger('prev.owl.carousel');
    });
});