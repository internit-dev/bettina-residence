<?php
require 'funcoes/device/device.php';

$device = detect_mobile();
$parameters = 186;

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>LP Bettina</title>
  <link rel="stylesheet" href="./assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="./assets/css/main.min.css">
  <link rel="stylesheet" href="./assets/css/owl.carousel.min.css">
  <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;700&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="./assets/css/fslightbox.min.css">
  <link rel="stylesheet" href="./assets/css/intlTelInput.min.css">
  <link rel="shortcut icon" href="./assets/img/favicon.png" type="image/x-icon">
  <meta name="facebook-domain-verification" content="47xfgukuu5hab2w4xvlsdrshnad3kl" />

  <script src="./assets/js/jquery.min.js"></script>
  <script src="./assets/js/jquery.mask.min.js"></script>
  <script src="./assets/js/owl.carousel.min.js"></script>
  <script src="./assets/js/intlTelInput.min.js"></script>
  <script src="./assets/js/bootstrap.min.js"></script>
  <script src="./assets/js/fslightbox.min.js"></script>
  <script src="./assets/js/main.js"></script>
  <script src="./assets/js/auxiliar.js"></script>
  <script src="./assets/js/Blocks/Lazer/lazer.js"></script>
  <script src="./assets/js/Blocks/Plantas/plantas.js"></script>
  <script src="./assets/js/processa.js"></script>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-N4NKS23');</script>
    <!-- End Google Tag Manager -->

</head>

<body>


  <!-- COMPONENTE DE MENU -->
  <?php include 'Blocks/Menu/Menu.php'; ?>

  <?php include 'Blocks/Banner/Banner.php'; ?>

  <main>
    <?php include 'Blocks/InfoEmpreendimento/InfoEmpreendimento.php'; ?>

    <?php include 'Blocks/Lazer/Lazer.php'; ?>

    <?php include 'Blocks/Localizacao/Localizacao.php'; ?>

    <?php include 'Blocks/Plantas/Plantas.php'; ?>

    <?php include 'Blocks/Realizacao/Realizacao.php'; ?>

    <?php include 'Blocks/Contato/Contato.php'; ?>

    <?php include 'Blocks/BarraComercial/BarraComercial.php'; ?>


    <!-- Aviso de Política de Privacidade -->
    <div onclick="jQuery('.modalPopup').fadeOut().remove();" class="modalPopup"  style="align-items: center; display:flex;justify-content:center;position:fixed;bottom:0;left:50%;min-width: 69%; height: auto;background:rgba(0,0,0,.7);z-index:999999; transform: translate(-50%,0); border-radius: 6px;">
      <div class="modal-main" style="z-index: 99;position: relative;width: 100%;padding: 0 30px;">
        <div class="modal-content" style="border: 0;background-color:unset; text-align: center; display:flex;">
          <span style="color: #FFF; text-align: center; padding-top:1rem;">Este site utiliza cookies para melhorar a experiência do usuário. Ao utilizar este site, você concorda com o uso de cookies. Para saber mais informações sobre os cookies utilizados acesse nossa <a href="https://bettinaresidence.com.br/politica-de-privacidade/" target="_blank">Política de Privacidade</a>.</span>
          <span class="modal-close activeCookies" onclick="jQuery('.modalPopup').fadeOut().remove();" style="text-align: center; color: #FFF; background: #029EE3; width: 10%; margin: 10px auto; border-radius: 10px; display: flex; justify-content: center; cursor:pointer;">Aceitar</span>
        </div>
      </div>
    </div>
  </main>

  <footer>
    <div class="container h-100">
      <div class="d-flex justify-content-center align-items-center h-100">
        <div class="row flex-grow-1 h-100">
          <div class="col-md-6">
            <div class="logo internit h-100 d-flex justify-content-center align-items-center">
              <a href="https://www.internit.com.br" target="_blank">
                <img src="./assets/img/logo-internit.png" alt="Logo da Internit" width="81%">
              </a>
            </div>
          </div>
          <div class="col-md-6">
            <div class="logo incena h-100 d-flex justify-content-center align-items-center">
              <a href="https://incenadigital.com.br/" target="_blank">
                <img src="./assets/img/logo-incena.svg" alt="Logo da Incena Digital" width="100%">
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer>
</body>

<!-- Modal -->
<div class="modal fade" id="contato-realizado" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Contato Realizado</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <br><br>
        <div class="text-center">
          Obrigado!
          <br>Em Breve Entraremos em Contato!
        </div>
        <br>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-N4NKS23"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->


<?php require_once 'rodizio/rodizio.php'; ?>

<?php require_once 'import-tags/importJs.php'; ?>

<?php require_once 'import-tags/importJsIntegration.php'; ?>


</html>